<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class FixtureRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $cant;
    private $participantes;
    public function __construct($cant)
    {
        $this->cant = $cant;
        $this->participantes = [];
        for ($i = 0; $i < $this->cant; $i++) {
            array_push($this->participantes, 0);
        }
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        try {
            if (!$this->checker($value)) {
                return false;
            }
            foreach ($this->participantes as $participante) {
                if ($participante != 1) {
                    // Su debut ocurre una vez
                    return false;
                }
            }
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error message.';
    }

    private function checker($match)
    {
        if (count($match) != 2) {
            return false;
        }
        foreach ($match as $jugador) {
            if (is_array($jugador)) {
                // Es el ganador de un encuentro previo
                if (!$this->checker($jugador)) {
                    return false;
                }
            } else {
                $this->participantes[$jugador]++;
            }
        }
        return true;
    }
}
