<?php

namespace App;

use App\Juego;
use Illuminate\Database\Eloquent\Model;

class Torneo extends Model
{
    protected $fillable = [
        'nombre', 'inicio', 'fin',
        'cantidad_participantes',
    ];

    protected $with = ['participantes', 'final'];

    public function greedyBracket()
    {
        if (is_null($this->final)) {
            $stack = $this->participantes;
            while ($stack->count() > 1) {
                $j1 = $stack->shift();
                $j2 = $stack->shift();
                $juego = new Juego([
                    'torneo_id' => $this->id,
                ]);
                $juego->jugador1()->associate($j1);
                $juego->jugador2()->associate($j2);
                $juego->save();
                $stack->push($juego);
            }
            $last = $stack->shift();
            $last->is_final = true;
            $last->save();
        }
        return $stack;
    }
    public function participantes()
    {
        return $this->hasMany("App\Participante");
    }

    public function juegos()
    {
        return $this->hasMany("App\Juego");
    }

    public function final()
    {
        return $this->hasOne("App\Juego", "torneo_id")
            ->where("is_final", "=", true);
    }
}
