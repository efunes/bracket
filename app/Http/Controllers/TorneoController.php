<?php

namespace App\Http\Controllers;

use App\Juego;
use App\Torneo;
use Illuminate\Http\Request;
use App\Http\Requests\StoreTorneoRequest as StoreRequest;
use App\Http\Requests\UpdateTorneoRequest as UpdateRequest;


class TorneoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $torneos = Torneo::query();

        return $torneos->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTorneoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $all = $request->all();
        $torneo = Torneo::create($all);
        $torneo->participantes()->createMany($all["participantes"]);
        $final = $this->storeJuegos($all['fixture'], $torneo);
        $final->is_final = true;
        $final->save();

        return $torneo;
    }

    private function storeJuegos($fixture, $torneo)
    {
        $juego = new Juego([
            'torneo_id' => $torneo->id,
        ]);
        $v0 = $fixture[0];
        $j1 = is_array($v0)
            ? $this->storeJuegos($fixture[0], $torneo)
            : $torneo->participantes->all()[$v0];

        $v1 = $fixture[1];
        $j2 = is_array($v1)
            ? $this->storeJuegos($v1, $torneo)
            : $torneo->participantes->all()[$v1];

        $juego->jugador1()->associate($j1);
        $juego->jugador2()->associate($j2);
        $juego->save();
        return $juego;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Torneo  $torneo
     * @return \Illuminate\Http\Response
     */
    public function show(Torneo $torneo)
    {
        return $torneo;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTorneoRequest  $request
     * @param  \App\Torneo  $torneo
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Torneo $torneo)
    {
        $all = $request->all();
        $torneo->update($all);
        return $torneo;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Torneo  $torneo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Torneo $torneo)
    {
        $torneo->juegos()->delete();
        $torneo->participantes()->delete();
        $torneo->delete();

        return $torneo;
    }
}
