<?php

namespace App\Http\Controllers;

use App\Participante;
use App\Http\Requests\UpdateParticipanteRequest as UpdateRequest;

class ParticipanteController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateParticipanteRequest  $request
     * @param  \App\Participante  $participante
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Participante $participante)
    {
        $all = $request->all();
        $participante->update($all);
        return $participante;
    }
}
