<?php

namespace App\Http\Controllers;

use App\Juego;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateJuegoRequest as UpdateRequest;

class JuegoController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateJuegoRequest  $request
     * @param  \App\Participante  $participante
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Juego $juego)
    {
        $all = $request->all();
        $juego->update($all);
        return $juego;
    }
}
