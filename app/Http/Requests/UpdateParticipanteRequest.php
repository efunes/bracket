<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateParticipanteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => ['string', "max:240"],
        ];
    }

    /**
     * Obtiene los campos validados.
     * 
     * @param  array|mixed|null  $keys
     * @return array
     */
    public function all($keys = null)
    {
        $rules = array_keys($this->rules());
        if (is_null($keys)) {
            $keys = $rules;
        } else {
            $keys = array_intersect($rules, $keys);
        }
        return parent::all($keys);
    }
}
