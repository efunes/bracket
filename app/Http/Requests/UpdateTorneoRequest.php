<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTorneoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => ['required', 'max:240'],
            'inicio' => ['required', 'date'],
            'fin' => ['required', 'date', 'after_or_equal:inicio'],
        ];
    }

    /**
     * Obtiene los campos validados.
     * 
     * @param  array|mixed|null  $keys
     * @return array
     */
    public function all($keys = null)
    {
        $rules = array_keys($this->rules());
        if (is_null($keys)) {
            $keys = $rules;
        } else {
            $keys = array_intersect($rules, $keys);
        }
        return parent::all($keys);
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nombre.required' => 'Se necesita un nombre.',
            'nombre.max' => 'El nombre es demasiado largo.',
            'inicio.required' => 'Se necesita una fecha de inicio.',
            'inicio.date' => 'La fecha de inicio no es válida.',
            'fin.required' => 'Se necesita una fecha de finalización.',
            'fin.date' => 'La fecha de finalización no es válida.',
            'fin.after_or_equal' => 'La fecha de finalización no debe ser menor a la fecha de inicio.',
        ];
    }
}
