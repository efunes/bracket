<?php

namespace App\Http\Requests;

use App\Rules\FixtureRule;
use App\Rules\ParRule;
use Illuminate\Foundation\Http\FormRequest;

class StoreTorneoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $cant = $this->cantidad_participantes;
        return [
            'nombre' => ['required', 'max:240'],
            'inicio' => ['required', 'date'],
            'fin' => ['required', 'date', 'after_or_equal:inicio'],
            'cantidad_participantes' => ['required', 'integer', 'min:2', 'max:128', new ParRule],
            'participantes' => ['array', "size:$cant"],
            'participantes.*.nombre' => ["required", "max:240"],
            'fixture' => ['array', new FixtureRule($cant)]
        ];
    }
    /**
     * 
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nombre.required' => 'Se necesita un nombre.',
            'nombre.max' => 'El nombre es demasiado largo.',
            'inicio.required' => 'Se necesita una fecha de inicio.',
            'inicio.date' => 'La fecha de inicio no es válida.',
            'fin.required' => 'Se necesita una fecha de finalización.',
            'fin.date' => 'La fecha de finalización no es válida.',
            'fin.after_or_equal' => 'La fecha de finalización no debe ser menor a la fecha de inicio.',
            'cantidad_participantes.min' => 'La cantidad mínima es de 2 participantes.',
            'cantidad_participantes.max' => 'La cantidad máxima es de 128 participantes.',
        ];
    }
}
