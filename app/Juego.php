<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Juego extends Model
{
    protected $fillable = [
        'torneo_id', 'fecha', 'resultado', 'ganador'
    ];

    protected $with = ['jugador1', 'jugador2'];
    protected $appends = ['ganador_nombre'];

    public function getGanadorNombreAttribute()
    {
        if ($this->ganador == "1") {
            return $this->jugador1->nombre ?: $this->jugador1->ganador_nombre;
        } else if ($this->ganador == "2") {
            return $this->jugador2->nombre ?: $this->jugador2->ganador_nombre;
        }
        return null;
    }

    public function jugador1()
    {
        return $this->morphTo();
    }
    public function jugador2()
    {
        return $this->morphTo();
    }

    public function torneo()
    {
        return $this->belongsTo("App\Torneo");
    }
}
