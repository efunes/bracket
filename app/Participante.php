<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participante extends Model
{
    protected $fillable = [
        'nombre',
    ];

    public function torneo()
    {
        $this->belongsTo("App\Torneo");
    }
}
