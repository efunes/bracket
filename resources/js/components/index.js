import modal from "./modal.vue";
import bracket from "./bracket.vue";
import loading from "./loading.vue";
import create_bracket from "./creating-bracket.vue";

export default {
  install(Vue, options) {
    Vue.component("modal", modal);
    Vue.component("bracket", bracket);
    Vue.component("loading", loading);
    Vue.component("creating-bracket", create_bracket);
  }
}