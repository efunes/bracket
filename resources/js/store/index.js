import Vue from "vue";
import Vuex from "vuex";
import torneo from "./modules/torneo"

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    torneo,
  }
});
