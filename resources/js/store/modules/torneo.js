import service from "../../services/torneo";

const state = {
  all: [],
};

const getters = {
  //
};

const actions = {
  list({ commit }, payload) {
    return service.list(payload)
      .then((payload) => {
        commit("all", payload);
        return payload;
      });
  },
  item({ }, { id, params }) {
    return service.item(id, params);
  },
  create({ }, { data, params }) {
    return service.create(data, params);
  },
  edit({ commit }, { id, data, params }) {
    return service.edit(id, data, params)
      .then((payload) => {
        commit("item", payload);
        return payload;
      });
  },
  delete({ }, { id, params }) {
    return service.delete(id, params);
  },
  edit_juego({ }, { id, data, params }) {
    return service.edit_juego(id, data, params);
  },
  edit_participante({ }, { id, data, params }) {
    return service.edit_participante(id, data, params);
  },
};

const mutations = {
  all(state, payload) {
    state.all = payload;
  },
  item(state, payload) {
    for (let i = 0; i < state.all.length; i++) {
      let item = state.all[i];
      if (item.id == payload.id) {
        state.all[i] = payload;
        return;
      }
    }
    state.all.push(payload);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
