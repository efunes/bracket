// Ejemplo Base de un Module de Store Vuex

const state = {
  //
};

const getters = {
  //
};

const actions = {
  //
};

const mutations = {
  //
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
