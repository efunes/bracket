require('./bootstrap');

import App from "./App.vue";
import store from "./store";
import router from "./router";
import components from "./components";

import Vue from "vue";

Vue.use(components);

const app = new Vue({
  store,
  router,
  render: h => h(App)
});


app.$mount("#app");
