import api from "./api";

export default {
  list(payload) {
    return api
      .get("/torneos", payload)
      .then((response) => response.data);
  },
  item(id, payload) {
    return api
      .get(`/torneos/${id}`, payload)
      .then((response) => response.data);
  },
  create(data, payload) {
    return api
      .post(`/torneos`, data, payload)
      .then((response) => response.data);
  },
  edit(id, data, payload) {
    return api
      .put(`/torneos/${id}`, data, payload)
      .then((response) => response.data);
  },
  edit_juego(id, data, payload) {
    return api
      .put(`/juegos/${id}`, data, payload)
      .then((response) => response.data);
  },
  edit_participante(id, data, payload) {
    return api
      .put(`/participantes/${id}`, data, payload)
      .then((response) => response.data);
  },
  delete(id, payload) {
    return api
      .delete(`/torneos/${id}`, payload)
      .then((response) => response.data);
  }
};
