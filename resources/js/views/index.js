import landing from "./landing.vue";
import torneo from "./torneos/item.vue";
import create_torneo from "./torneos/create.vue";

export default [
  {
    path: "/",
    name: "landing",
    component: landing,
    meta: {
      //
    }
  },
  {
    path: "/torneo/create",
    name: "torneo.create",
    component: create_torneo,
    meta: {
      //
    }
  },
  {
    path: "/torneo/:id",
    name: "torneo.item",
    component: torneo,
    meta: {
      //
    }
  },
];