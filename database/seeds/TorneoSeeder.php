<?php

use Illuminate\Database\Seeder;

class TorneoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $torneos = factory(App\Torneo::class, 1)
            ->create()
            ->each(function (App\Torneo $torneo) {
                $torneo
                    ->participantes()
                    ->createMany(
                        factory(
                            App\Participante::class,
                            $torneo->cantidad_participantes
                        )
                            ->make()
                            ->toArray()
                    );
                $torneo->greedyBracket();
            });;
    }
}
