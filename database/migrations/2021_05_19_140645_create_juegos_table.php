<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJuegosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('juegos', function (Blueprint $table) {
            $table->id();
            $table->date("fecha")
                ->nullable()
                ->default(null);
            $table->string("resultado")
                ->nullable()
                ->default(null);
            $table->string("ganador", 1)
                ->default("");
            $table->boolean("is_final")
                ->default(false);

            $table->morphs("jugador1");
            $table->morphs("jugador2");

            $table->unsignedBigInteger('torneo_id');
            $table->foreign('torneo_id')->references('id')->on('torneos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('juegos');
    }
}
