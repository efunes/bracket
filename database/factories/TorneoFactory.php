<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Torneo;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;

$factory->define(Torneo::class, function (Faker $faker) {
    $date = $faker->date();
    $date2 = (new Carbon($date))->addMonth();
    return [
        'nombre' => "Torneo de Ejemplo",
        'inicio' => $date,
        'fin' => $date2,
        'cantidad_participantes' => 8
    ];
});
